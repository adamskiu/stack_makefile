#include "stack.h"

#define STACK_SIZE 20

static int stack[STACK_SIZE];
static int pos = 0;

int push(int number)
{
	if(pos < STACK_SIZE ){
		stack[pos++] = number;
		return 1;
	}
	return 0;
}

int top(void)
{
	return stack[pos-1];
}

int pop(void)
{
	if (pos > 1){
		pos--;
		return 1;
	}
	return 0;
}
