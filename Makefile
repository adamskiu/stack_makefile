OUT = stack
CC = gcc
LIBDIR = src
SOURCES = main.c
OBJECTS = main.o
CFLAGS = -Wall -Iinclude -c
LDFLAGS = -lstack -L$(LIBDIR)
LIBFILE = src/libstack.so

all : $(OUT)

$(OUT) : $(OBJECTS) $(LIBFILE) 
	$(CC) $(OBJECTS) $(LDFLAGS) -o $(OUT)

$(OBJECTS) : $(SOURCES)
	$(CC) $(CFLAGS) $(SOURCES)

$(LIBFILE) : 
	make -C $(LIBDIR)

clean:
	rm -f $(OBJECTS) $(OUT)
	make clean -C$(LIBDIR)
